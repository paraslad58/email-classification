#!/usr/bin/env python
# coding: utf-8

import io
import os
from pandas import DataFrame
import nltk
from nltk.corpus import stopwords
nltk.download('stopwords')
import string
import numpy as np
import time


# fetching data 

def rF(loc):
    for rt, dnames,fnames in os.walk(loc):
        for fname in fnames:
            loc = os.path.join(rt, fname)
            
            iB = False
            lines = []
            f = io.open(loc, 'rb')
            for line in f:
                if iB:
                    lines.append(line)
                elif line == '\n':
                    iB = True
            f.close()
            mes = '\n'.join(lines)
            yield loc,mes

def dataFromFolders(loc, classify):
    r = []
    ind = []
    for fname, mes in rF(loc):
        r.append({'mes' : mes, 'class' : classify})
        ind.append(fname)
    
    return DataFrame(r, index=ind)

#fetching data for training

trn_data0 = DataFrame({'mes' : [], 'class' : []})
trn_data1 = DataFrame({'mes' : [], 'class' : []})

trn_data0 = trn_data0.append(dataFromFolders('comp.sys.ibm.pc.hardware.train', 'hardware'))
trn_data1 = trn_data1.append(dataFromFolders('sci.electronics.train', 'electronics'))

#dimension of data
len_hardware = trn_data0.shape[0]
len_electronics = trn_data1.shape[0]
len_trndata = len_hardware + len_electronics


#Priors

P_hardware = len_hardware / len_trndata
P_electronics = len_electronics/ len_trndata


#tokeizer and removing stopwords

def cleantxt(txt):
    
    rm_p = [ch for ch in txt if ch not in string.punctuation]
    rm_p = ''.join(rm_p)
    
    clntxt = [w for w in rm_p.split() if w.lower() not in stopwords.words('english')]
    
    clntxt = (list(set(clntxt)))
    
    return clntxt


#class hardware fetching data and creating tokens

lis_hardware = trn_data0['mes']
lis_hardware_tokens = cleantxt(lis_hardware)

u =[]
lis_hardware = trn_data0['mes']
for i in lis_hardware:
    u.extend(cleantxt(i))   


#class clectronics fetching data amd creating tokens

lis_electronics = trn_data1['mes']
lis_electronics_tokens = cleantxt(lis_electronics)

v =[]
lis_electronics = trn_data1['mes']
for i in lis_electronics:
    v.extend(cleantxt(i))


#mergining data from both class

lis_hw = lis_hardware_tokens
lis_ec = lis_electronics_tokens
lis_up = lis_hw + lis_ec


#remove duplicates

lis_up_fin = (list(set(lis_up)))


# initilising small constant

k = 0.1
den_k = 2*k
lis_Y1 = []
lis_Y2 = []


# Applying Naive Bayes Classifier

for j in lis_up_fin:
    count = 0
    for s in range(len(u)):
        if j == u[s]:
            count +=1
    P_XY1= (count+k)/(len(lis_hardware)+den_k)
    lis_Y1.append(P_XY1)


for j in lis_up_fin:
    count1 = 0
    for t in range(len(v)):
        if j == v[t]:
            count1 +=1
    P_XY2= (count1+k)/(len(lis_electronics)+den_k)
    lis_Y2.append(P_XY2)




#Testing


# importing testing data

test_data0 = DataFrame({'mes' : [], 'class' : []})
test_data1 = DataFrame({'mes' : [], 'class' : []})

test_data0 = test_data0.append(dataFromFolders('comp.sys.ibm.pc.hardware.test', 'hardware'))
test_data1 = test_data1.append(dataFromFolders('sci.electronics.test', 'electronics'))


#class hardware testing

lis_hardware_test = test_data0['mes']
lab_test_hw = [0]*len(lis_hardware_test)


#class electronics testing

lis_electronics_test = test_data1['mes']
lab_test_ec = [1]*len(lis_electronics_test)


# combining output into a list

act_output = lab_test_hw + lab_test_ec


# Function of Naive Bayes classifier to predict class

def classify(dat): 
    for i in dat:
        i_up = cleantxt(i)
        lis_prob1 = []
        lis_prob2 = []
        for j in i_up:
            for k in range(len(lis_up_fin)):
                if j == lis_up_fin[k]:
                    lis_prob1.append(lis_Y1[k])
                    lis_prob2.append(lis_Y2[k])

        mul_Y1 = 0
        for l in lis_prob1:
            mul_Y1 +=np.log(l) 
        mul_Y1_up= np.exp(mul_Y1)
        output0 = mul_Y1_up*P_hardware

        mul_Y2 = 0
        for k in lis_prob2:
            mul_Y2 +=np.log(k)
        mul_Y2_up = np.exp(mul_Y2)
        output1 = mul_Y2_up*P_electronics

        if output0>output1:
            lis_output.append(0)
        else:
            lis_output.append(1)


# Applying function to data

start = time.time()
lis_output =[]
classify(lis_hardware_test)
classify(lis_electronics_test)
end = time.time()


# Calculating accuracy


mis_classified = 0
for h in range(len(act_output)):
    if act_output[h]!=lis_output[h]:
        mis_classified +=1
acc = 1 - mis_classified/len(act_output)
print("Testing Accuracy : ",acc)
